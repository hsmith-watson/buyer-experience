import { createClient } from 'contentful';

const config = {
  space: process.env.CTF_SPACE_ID,
  accessToken: process.env.CTF_CDA_ACCESS_TOKEN,
  previewToken: process.env.CTF_PREVIEW_ACCESS_TOKEN,
};

let client;

// Obtain a Singleton instance of the client
export const getClient = () => {
  if (!client) {
    client = createClient(config);
  }

  return client;
};
