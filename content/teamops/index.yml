---
  title: 'TeamOps: Optimizing Team Efficiency | GitLab'
  og_title: 'TeamOps: Optimizing Team Efficiency | GitLab'
  description: TeamOps is a results-focused team management discipline that reduces decision-making blockers to ensure fast and efficient strategic executions. Learn more!
  twitter_description: TeamOps is a results-focused team management discipline that reduces decision-making blockers to ensure fast and efficient strategic executions. Learn more!
  og_description: TeamOps is a results-focused team management discipline that reduces decision-making blockers to ensure fast and efficient strategic executions. Learn more!
  og_image: /nuxt-images/open-graph/teamops-opengraph.png
  twitter_image: /nuxt-images/open-graph/teamops-opengraph.png
  hero:
    logo:
        show: true
    title: |
      Stay Connected.
      Stay Productive.
    subtitle: Redefining teamwork for the modern era
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 200
    image:
      url: /nuxt-images/team-ops/hero-illustration.png
      alt: team ops hero image
      aos_animation: zoom-out-left
      aos_duration: 1600
      aos_offset: 200
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Learn How
      data_ga_name: enroll
      data_ga_location: hero
  spotlight:
    title: |
      TeamOps: A Framework for Operating in the Future of Work
    subtitle: Navigating the challenges of our evolving work dynamics? TeamOps paves the way for streamlined decisions and seamless execution, fostering top-tier teams that make remarkable progress. 
    description:
      TeamOps is a performance-centric operational approach designed to optimize team dynamics, streamline decision-making, and enhance productivity within organizations.\n\n\n
      Developed, practiced, and refined by GitLab, it's a framework grounded in actionable principles that transform how teams work and relate.
    list:
      title: TeamOps resolves the common challenges teams encounter, such as...
      items:
        - Decision making delays
        - Meeting fatigue
        - Internal miscommunication
        - Slow handoffs and workflow delays
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Unlock TeamOps for Free
      data_ga_name: Unlock TeamOps for Free
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  features:
      title: |
        Built for all teams.
        Remote, Hybrid, or Office.
      image:
        url: /nuxt-images/team-ops/reasons-to-believe.png
        alt: Reasons to Believe in TeamOps image
      accordion:
        is_accordion: false
        items:
            - icon:
                name: group
                alt: User Group Icon
                variant: marketing
              header: TeamOps is an organizational operating model that helps teams maximize productivity, flexibility, and autonomy by managing information more efficiently.
              text: TeamOps helps organizations make greater progress, by optimizing knowledge management, decision making, and worker autonomy.
            - icon:
                name: clipboard-check-alt
                alt: Clipboard Checkmar Icon
                variant: marketing
              header: Proven in Practice
              text: GitLab's historical success with TeamOps boosts productivity and team morale. Originated internally, we are confident in its transformative power for businesses globally.
            - icon:
                name: principles
                alt: Continuous Integration Icon
                variant: marketing
              header: Operational Outcomes
              text: TeamOps is grounded in four Guiding Principles that can help organizations rationally navigate the dynamic, changing nature of work.
            - icon:
                name: cog-user-alt
                alt: Cog User Icon
                variant: marketing
              header: Strategies in Action
              text: Every principle is supported by a set of Action Tenets – practical working methods ready for immediate action.
            - icon:
                name: case-study-alt
                alt: Case Study Icon
                variant: marketing
              header: Real-World Examples
              text: We bring the Action Tenets to life with a growing library of real output-based examples from GitLab and other TeamOps practitioners.
            - icon:
                name: verification
                alt: Ribbon Check Icon
                variant: marketing
              header: Learning Services
              text: Get support in adopting TeamOps model from courses, workshops, certifications, assessments, consulting, and more.
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 200
      accordion_aos_animation: fade-left
      accordion_aos_duration: 1600
      aaccordion_os_offset: 200
  video_spotlight:
    title: |
      Get immersed in TeamOps
    subtitle: The world has plenty of opinions on the future of work.
    description:
      "The TeamOps Course helps organizations make greater progress, by treating how your team members relate as a problem that can be operationalized.\n\n\n
      In just a few hours, our free course will guide you through each of the Guiding Principles and Action Tenets of the model, and start to assess compatibility with your team’s current operating practices."
    video:
      url: 754916142?h=56dd8a7d5d
      alt: teamops hero image
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Enroll now
      data_ga_name: enroll your team
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  card_section:
    title: |
      Guiding Principles of TeamOps
    subtitle: TeamOps facilitates effective collaboration through four phases of group alignment.
    cards:
      - title: Shared reality
        description: |
          While other management philosophies prioritize the speed of knowledge transfer, TeamOps optimizes for the speed of knowledge retrieval.
        icon:
          name: d-and-i
          slp_color: surface-700
        link:
          text: Learn More
          url: https://handbook.gitlab.com/teamops/shared-reality/
        color: '#FCA326'
      - title: Everyone contributes
        description: |
          Organizations must create a system where everyone can consume information and contribute, regardless of level, function, or location.
        icon:
          name: user-collaboration
          slp_color: surface-700
        link:
          text: Learn More
          url: https://handbook.gitlab.com/teamops/everyone-contributes/
        color: '#966DD9'
      - title: Decision velocity
        description: |
          Success is correlated with decision velocity: the quantity of decisions made in a particular stretch of time and the results that stem from faster progress.
        icon:
          name: speed-alt-2
          slp_color: surface-700
        link:
          text: Learn More
          url: https://handbook.gitlab.com/teamops/decision-velocity/
        color: '#FD8249'
      - title: Measurement clarity
        description: |
          This is about measuring the right things. TeamOps’ decision-making principles are only useful if you execute and measure results.
        icon:
          name: target
          slp_color: surface-700
        link:
          text: Learn More
          url: https://handbook.gitlab.com/teamops/measurement-clarity/
        color: '#256AD1'
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  join_us:
    title: |
      Join the movement
    description:
      "TeamOps is an organizational operating model that helps teams maximize productivity, flexibility, and autonomy by managing decisions, information, and tasks more efficiently. \n\n\n
      Join a growing list of organizations who are practicing TeamOps."
    list:
      title: Common pain points
      items:
        - Ad hoc workflows prevent alignment
        - DIY Management breeds dysfunction
        - Communication infrastructure is an afterthought
        - Obsession over consensus thwarts innovation
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Enroll now
      data_ga_name: enroll
      data_ga_location: join the movement
    quotes:
      - text: Pre-deployment tests have provided more confidence that the product is ready to be released; also delivery frequency has increased.
        author: John Lastname
        note: Director of Job Title, Company name
      - text: Pre-deployment tests have provided more confidence that the product is ready to be released; also delivery frequency has increased.
        author: John Lastname
        note: Director of Job Title, Company name
    clients:
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
      - logo: /nuxt-images/home/logo_siemens_mono.svg
        alt: Siemens logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/team-ops/logo_knowbe4_mono.svg
        alt: KnowBe4 logo
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
