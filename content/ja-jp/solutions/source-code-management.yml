template: feature
title: 'ソースコード管理'
description: 'GitLabのソースコード管理（SCM）は、開発チームがコラボレーションを通じて生産性を最大限に高めるのに役立ち、ソフトウェア提供のスピードアップおよび可視性の向上が促進されます。'
components:
  feature-block-hero:
    data:
      title: ソースコード管理
      subtitle: GitLabでソースコード管理が簡単に
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        text: 無料トライアルを開始
        url: /free-trial/
      image:
        image_url: "/nuxt-images/devops-graphics/create.png"
        hide_in_mobile: true
        alt: ""
  side-navigation:
    links:
      - title: 概要
        href: '#overview'
      - title: メリット
        href: '#benefits'
    data:
      feature-overview:
        data:
          content_summary_bolded: GitLabのバージョン管理
          content_summary: >-
            は、開発チームがコラボレーションを通じて生産性を最大限に高めるのに役立ち、ソフトウェア提供のスピードアップおよび可視性の向上が促進されます。Gitベースのリポジトリを使用するGitLabを使用すれば、明確なコードレビュー、アセットのバージョン管理、フィードバックループ、および強力なブランチパターンを実現できるため、デベロッパーが問題を解決したり、価値を提供したりするのに役立ちます。
          content_image: /nuxt-images/features/source-code-management/overview.jpg
          content_heading: すべての人のためのバージョン管理
          content_list:
            - クラウドネイティブのアドプションに向けてソフトウェア開発ライフサイクル（SDLC）をスケール可能
            - Gitベースのリポジトリを使用しているため、デベロッパーがローカルコピーから作業可能
            - コミットごとにコード品質とセキュリティを自動スキャン
            - ビルトインの継続的インテグレーションと継続的デリバリ
      feature-benefit:
        data:
          feature_heading: ソフトウェア開発を変革
          feature_cards:
            - feature_name: コラボレーション
              icon:
                name: collaboration
                variant: marketing
                alt: コラボレーションアイコン
              feature_description: >-
                提供スピードをアップ... デベロッパーチームがコラボレーションを通じて生産性を最大限に高めるのに役立ち、ソフトウェア提供のスピードアップおよび可視性の向上が促進されます。
              feature_list:
                - 'レビュー、コメント、コードの改善'
                - 再利用とインナーソーシングが可能に
                - ファイルロックによる競合の防止
                - 堅牢なWebIDEによって、あらゆるプラットフォームにおける開発を加速
            - feature_name: 加速
              icon:
                name: increase
                variant: marketing
                alt: 増加アイコン
              feature_description: >-
                常に起動状態... アセットのバージョン管理、フィードバックループ、および強力なブランチパターンを実現できるため、デベロッパーが問題を解決したり、価値を提供したりするのに役立ちます。
              feature_list:
                - Gitベースのリポジトリを使用しているため、デベロッパーがローカルコピーから作業可能
                - 'コードのブランチを作成し、変更を加えた後に、メインブランチへのマージが可能'
                - 堅牢なWebIDEによって、あらゆるプラットフォームにおける開発を加速
            - feature_name: コンプライアンスとセキュリティ
              icon:
                name: release
                variant: marketing
                alt: Shield Check Icon
              feature_description: >-
                追跡とトレース... チームは信頼できる唯一の情報源を活用して作業を管理できます。
              feature_list:
                - >-
                  高性能なマージリクエストを使用して、コード変更のレビュー、追跡、承認が可能
                - コミットごとにコード品質とセキュリティを自動スキャン
                - >-
                  詳細なアクセスコントロールおよびレポートにより、監査とコンプライアンスを簡素化
  feature-block-related:
    data:
      - 継続的インテグレーション
      - エピックおよびイシューボード
      - セキュリティダッシュボード
      - 承認ルール
      - 脆弱性管理
  group-buttons:
    data:
      header:
        text: GitLabは、他にもソースコード管理に役立つソリューションを提供しています。
        link:
          text: その他のソリューションを確認
          href: /solutions/
      buttons:
        - text: デリバリの自動化
          icon_left: automated-code
          href: /solutions/delivery-automation/
        - text: 継続的インテグレーション
          icon_left: continuous-delivery
          href: /solutions/continuous-integration/
        - text: 継続的なソフトウェアセキュリティ
          icon_left: devsecops
          href: /solutions/continuous-software-security-assurance/
  report-cta:
    layout: "dark"
    title: アナリストレポート
    reports:
    - description: "GitLabがForrester Wave™: Integrated Software Delivery Platforms, Q 2 2023の唯一のリーダーに認定"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: レポートを読む
    - description: "DevOpsプラットフォームの2023 Gartner® Magic Quadrant™でGitLabがリーダーに認定"
      url: /gartner-magic-quadrant/
      link_text: レポートを読む
  solutions-resource-cards:
    data:
      title: リソース
      link:
        text: すべてのリソースを表示する
      cards:
        - icon:
            name: webcast
            variant: marketing
            alt: ウェブキャストアイコン
          event_type: Webcast
          header: 境界線のないコラボレーション - GitLabでより速いデリバリを実現
          link_text: 詳細はこちら
          image: /nuxt-images/features/resources/resources_webcast.png
          href: /webcast/collaboration-without-boundaries/
          data_ga_name: Collaboration without Boundaries
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: ケーススタディアイコン
          event_type: Case Study
          header: >-
            GitLabがヴィクトリア大学ウェリントン(テヘレンガワカ校)でオープンサイエンス教育を推進
          link_text: 詳細はこちら
          href: /customers/victoria_university/
          image: /nuxt-images/features/resources/resources_case_study.png
          data_ga_name: GitLab advances open science education at Te Herenga Waka
          data_ga_location: body
        - icon:
            name: partners
            variant: marketing
            alt: Partners Icon
          event_type: Partners
          header: GitLab on AWSの利点を見つける
          link_text: 今すぐ視聴
          href: /partners/technology-partners/aws/
          image: /nuxt-images/features/resources/resources_partners.png
          data_ga_name: Discover the benefits of GitLab on AWS
          data_ga_location: body
