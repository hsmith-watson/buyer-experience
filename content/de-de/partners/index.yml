---
  title: Cloud-, Plattform- und Technologie-Allianzpartner
  description: Erfahre mehr über GitLab-Partner – wer sie sind, welche Dienstleistungen sie erbringen und wie du Partner werden kannst.
  components:
    - name: 'hero'
      data:
        title: GitLab-Partnerprogramm
        text: „Wenn du schnell gehen willst, gehe alleine. Wenn du weit gehen willst, gehe mit jemandem zusammen.“ – Redewendung
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Jetzt bewerben
          url: https://partners.gitlab.com/English/register_email.aspx
          data_ga_name: apply today
          data_ga_location: header
        secondary_btn:
          text: Einen Vertriebspartner finden
          url: https://partners.gitlab.com/English/directory/search?f0=Partner+Certifications&f0v0=Professional+Services&l=United+States&Adv=none
          data_ga_name: find a channel partner
          data_ga_location: header
        tertiary_btn:
          text: Einen Allianzpartner finden
          url: /partners/technology-partners/
          data_ga_name: find an alliance partner
          data_ga_location: header
        image:
          url: /nuxt-images/solutions/infinity-icon-cropped.svg
          alt: "Bild: GitLab-Partner"
    - name: 'copy-block'
      data:
        header: Übersicht
        anchor_id: Overview
        top_margin: slp-mt-md-64
        column_size: 9
        blocks:
          - text: |
              GitLab arbeitet mit einem globalen Ökosystem führender Partner zusammen, um die wachsenden Bedürfnisse unserer Kunden in den Bereichen DevSecOps und digitale Transformation zu unterstützen. Durch unser Engagement für eine offene Zusammenarbeit haben wir ein Partnernetzwerk mit vollständigen Integrationen, Support und Dienstleistungen für den gesamten Software-Lebenszyklus geschaffen.
    - name: 'copy-block'
      data:
        header: Vertriebs-, Wiederverkaufs-, Integrations- und Schulungspartner
        anchor_id: channel-resell-integration-and-training-partners
        column_size: 9
        blocks:
          - text: |
              Die globalen Vertriebs- und Integrationspartner von GitLab helfen Kunden dabei, technische und geschäftliche Ziele im Hinblick auf die digitale Transformation zu erreichen. Wir arbeiten mit führenden Lösungsanbietern zusammen – einschließlich Systemintegratoren, Cloud-Plattformpartnern, Resellern, Distributoren, Managed-Service-Anbietern und Ökosystempartnern – um den Wert zu maximieren, den Kunden aus der Übernahme von GitLab als ihre DevSecOps-Plattform ziehen. Unsere Vertriebspartner sind berechtigt, Lizenzen im Namen von Endbenutzern weiterzuverkaufen oder zu erwerben und/oder Bereitstellungs-, Integrations-, Optimierungs-, Verwaltungs- und Schulungsdienste für GitLab-Kunden zu erbringen.

              Diese Lösungen sind ein wichtiger Bestandteil der Mission von GitLab, unseren Kunden moderne softwaregesteuerte Erfahrungen zu bieten. Gemeinsam mit unseren Partnern unterstützen wir Unternehmen und Organisationen jeder Größe und Branche auf der ganzen Welt dabei, die digitale Transformation voranzutreiben, die notwendig ist, um effektiver zu arbeiten und gleichzeitig ein großartiges Kundenerlebnis zu bieten.
    - name: 'partners-showcase'
      data:
        padding: 'slp-my-32 slp-my-md-48'
        items:
          - image:
              src: /nuxt-images/partners/badges/gitlab-select-partner-badge.svg
              alt: select partner badge
            title: Select Channel Resellers
            text: Select Partners sind Partner, die stärker in GitLab-Expertise investieren, Service-Praktiken rund um GitLab entwickeln und voraussichtlich mehr wiederkehrende Umsätze mit GitLab-Produkten erzielen werden. Die Teilnahme am Select Partner Track erfolgt nur auf Einladung von GitLab.
          - image:
              src: /nuxt-images/partners/badges/gitlab-open-partner-badge.svg
              alt: open partner badge
            title: Open Channel Resellers
            text: Unser Open Track steht allen Partnern zur Verfügung, welche die Mindestanforderungen erfüllen und GitLab-Verkaufschancen identifizieren oder unterstützen wollen. GitLab Open Partners können Transaktionspartner sein und können Produktrabatte oder Empfehlungsgebühren erhalten. Wiederverkäufer, Integratoren und andere Vertriebs- und Servicepartner treten dem Programm im Open Track bei.
          - image:
              src: /nuxt-images/partners/badges/gitlab-professional-services-partner-badge.svg
              alt: professional services partner badge
            title: Globale und regionale Systemintegratoren und Professional Services
            text: Unterstütze deine Implementierung und Einführung mit Bereitstellungs-, Integrations- und Optimierungsservices. Diese Partner können auch GitLab-Software weiterverkaufen.
    - name: 'copy-block'
      data:
        no_decoration: true
        column_size: 9
        blocks:
          - text: |
              GitLab bietet auch Partnerzertifizierungen an, mit denen Partner ihre GitLab-Expertise vertiefen können. Die GitLab Professional Services Partner-Zertifizierung ermöglicht es ihnen, sich mit einzigartigen Serviceangeboten von der Masse abzuheben und die Akzeptanz der GitLab-Plattform zu fördern. Darüber hinaus können GitLab Certified Training Partners GitLab- oder kundenspezifische Schulungen anbieten, um Kunden dabei zu unterstützen, mehr Fachwissen in Bezug auf die Verwendung von GitLab zu entwickeln.
            link:
              text: Einen Vertriebspartner finden
              url: https://partners.gitlab.com/English/directory/search?f0=Partner+Certifications&f0v0=Professional+Services&l=United+States&Adv=none
              data_ga_name: find a partner

    - name: 'copy-block'
      data:
        header: Cloud-, Plattform- und Technologie-Allianzpartner
        anchor_id: cloud-platform-and-technology-alliance-partners
        column_size: 9
        blocks:
          - text: |
              Wir arbeiten mit branchenführenden Cloud- und Technologieanbietern in allen wichtigen Branchen zusammen, um die beste kuratierte moderne DevSecOps-Plattform bereitzustellen. Unsere Partner integrieren GitLab, um maßgeschneiderte DevSecOps-Lösungen für alle Branchen und Anwendungsfälle bereitzustellen. Sie sind ein wichtiger Bestandteil der Mission von GitLab: unseren Kunden moderne softwaregesteuerte Erlebnisse zu ermöglichen und sicherzustellen, dass „jeder seinen Beitrag leisten kann“ – durch ein robustes und florierendes Partner-Ökosystem, das Innovationen fördert und die Transformation anregt. Gemeinsam ermöglichen wir unseren Unternehmenskunden, die erforderliche digitale Transformation durchzuführen, um effektiv auf dem modernen Markt zu konkurrieren.
            link:
              text: Einen Allianzpartner finden
              url: /partners/technology-partners/
              data_ga_name: find an alliance partner
    - name: 'partners-showcase'
      data:
        padding: 'slp-mt-32 slp-mt-md-48'
        clickable: true
        items:
          - image:
              src: /nuxt-images/partners/badges/gitlab-cloud-partner-badge.svg
              alt: cloud partner badge
            text: Cloud-Serviceprovider und Hyperscaler, die öffentliche Cloud-Computing-Services und Software-Marktplätze bereitstellen, über die Kunden GitLab beziehen und einsetzen können. Wir haben die Bereitstellung von GitLab vereinfacht, indem wir mit führenden Cloud-Anbietern zusammenarbeiten, um bessere Software schneller bereitzustellen. Unsere Cloud-nativen Integrationen bilden eine direkte Verbindung zu den Umgebungen, denen Entwickler(innen) am meisten vertrauen.
            url:
              href: /partners/technology-partners/#cloud-partners
              ga:
                data_ga_name: cloud partner
                da_ga_lcoation: body
          - image:
              src: /nuxt-images/partners/badges/gitlab-platform-partner-badge.svg
              alt: platform partner badge
            text: Hardware- und Software-Partner, die moderne Cloud-/Cloud-native Anwendungsplattformen bereitstellen, welche die Modularität und Erweiterbarkeit von GitLab über Unternehmen und Architekturen hinweg erhöhen.
            url:
              href: /partners/technology-partners/#platform-partner
              ga:
                data_ga_name: platform partner
                da_ga_lcoation: body
          - image:
              src: /nuxt-images/partners/badges/gitlab-technology-partner-badge.svg
              alt: technology partner badge
            text: Unabhängige Softwareanbieter (Independent Software Vendors, ISV) mit ergänzenden Technologien, die sich nahtlos in GitLab integrieren/mit GitLab interagieren, um Kundenlösungen zu vervollständigen.
            url:
              href: /partners/technology-partners/#technology-partners
              ga:
                data_ga_name: technology partner
                da_ga_lcoation: body
    - name: 'copy-block'
      data:
        header: Erfolgreiche Partnerschaften vorantreiben
        anchor_id: driving-successful-partnerships
        column_size: 9
        blocks:
          - text: |
              __„Wenn du schnell gehen möchtest, gehe allein. Wenn du weit gehen möchtest, gehe mit jemandem zusammen.“__

              Dieses Sprichwort beschreibt unser Engagement für gemeinsamen Erfolg perfekt. GitLab hat ein robustes Angebot für die Befähigung von Partnern, Schulungen und kommerzielle Programme entwickelt, um unserem Ökosystem von Partnern und Kunden die Möglichkeit zu geben, den vollen Nutzen aus DevSecOps und Investitionen in die digitale Transformation zu ziehen.
            link:
              url: /partners/benefits/
              text: GitLab-Partnervorteile anschauen
              data_ga_name: see gitlab partner benefits
    - name: 'quotes-carousel'
      data:
        no_background: true
        quotes:
          - main_img:
              url: /nuxt-images/partners/headshots/HeadshotMichaelMcBride.png
              alt: Foto von Michael McBride, Chief Revenue Officer bei GitLab
            quote: |
              „Wir sehen eine enorme Chance, über 100.000 Unternehmen dabei zu helfen, ihre vielen DevSecOps-Tools zu konsolidieren und zu einem Plattformansatz zu wechseln. Wir arbeiten mit unseren Partnern zusammen, um den Kundenerfolg mit der DevSecOps-Plattform von GitLab zu beschleunigen, indem wir Services bereitstellen, welche die Tools, die Kultur und die Prozesse von Organisationen verändern.“
            author: Michael McBride
            job: Chief Revenue Officer bei GitLab
    - name: 'resources'
      data:
        header: Partnerlösungen
        links:
          - name: Professional Services
            link: /services/
            description: |
              Implementierungs-, Bereitstellungs-, Integrations- und Optimierungsservices
            ga:
              name: professional services
              location: body
          - name: Managed Services (MSP)
            link: https://partners.gitlab.com/English/directory/search?f0=Services+Offered&f0v0=Managed+%2F+Hosted+Services&Adv=none
            description: |
              Schlüsselfertiger, verwalteter GitLab-Hosting-Service für individuelle Anforderungen
            ga:
              name: managed (msp) services
              location: body
          - name: Schulungs-Services
            link: https://partners.gitlab.com/English/directory/search?f0=Services+Offered&f0v0=Training&Adv=none
            description: |
              Formale Zertifizierung und maßgeschneiderte Schulungen
            ga:
              name: training services
              location: body
          - name: Partnerzertifizierungen
            link: /handbook/resellers/services/
            description: |
              Lass dich professionell ausbilden und entwickle dich weiter als Weiterverkäufer-, PS-, MSP- und Schulungspartner von GitLab.
            ga:
              name: partner certifications
              location: body
          - name: Partnerportal
            link: https://partners.gitlab.com/English/
            description: |
              Zugang zu unserem exklusiven Partnerportal für einzigartige Ressourcen, Lösungen und die Registrierung von Geschäften
            ga:
              name: partner portal
              location: body
          - name: Entwickle dich weiter
            link: https://partners.gitlab.com/English/?ReturnUrl=/prm/English/c/Training
            description: |
              Erlerne neue Fähigkeiten und erweitere deine beruflichen Möglichkeiten (Anmeldung im Partnerportal für den Zugang erforderlich).
            ga:
              name: level up
              location: body
          - name: Schulungen, Zertifizierungen und Kompetenzen für Partner
            link: /handbook/resellers/training/
            description: |
              Rollenbasierte Schulungen und Kompetenzen, um deine GitLab-Fähigkeiten zu erweitern
            ga:
              name: partner training, certifications, and enablement
              location: body
          - name: Professionelle Zertifizierungen
            link: /learn/certifications/public/
            description: |
              Öffentlich verfügbare Zertifizierungen für GitLab-Anwender(innen) und Fachleute
            ga:
              name: professional certifications
              location: body
          - name: Technologieintegration & Onboarding
            link: /partners/technology-partners/integrate/
            description: |
              Neue Partner können mit GitLab integrierte/interoperable Lösungen erstellen.
            ga:
              name: technology integration & on-boarding
              location: body
    - name: 'copy-block'
      data:
        header: Lerne unsere Top-Partner kennen
        anchor_id: meet-our-featured-partners
        column_size: 9
        blocks:
          - text: |
              Wir haben den Einstieg in GitLab vereinfacht, indem wir mit führenden Cloud-, DevSecOps-, Technologie-, Lösungs-, Wiederverkaufs- und Schulungspartnern zusammenarbeiten, um dir zu helfen, bessere Software schneller bereitzustellen. Unsere GitLab-fähigen Integrationen bilden eine direkte Verbindung zu den Umgebungen und Tools, denen Entwickler(innen) am meisten vertrauen.
    - name: 'intro'
      data:
        as_cards: true
        logos:
          - name: VMware Tanzu
            image: /nuxt-images/partners/vmware/logo-vmware-tanzu-square.jpg
            url: /partners/technology-partners/vmware-tanzu/
            aria_label: Link zur Partner-Fallstudie von VMware Tanzu
          - name: IBM
            image: /nuxt-images/partners/ibm/ibm.png
            url: /partners/technology-partners/ibm/
            aria_label: Link zur Partner-Fallstudie von IBM
          - name: Redhat
            image: /nuxt-images/partners/redhat/redhat_logo.svg
            url: /partners/technology-partners/redhat/
            aria_label: Link zur Partner-Fallstudie von Redhat
          - name: Hashicorp
            image: /nuxt-images/partners/hashicorp/hashicorp.svg
            url: /partners/technology-partners/hashicorp/
            aria_label: Link zur Partner-Fallstudie von Hashicorp
          - name: GCP
            image: /nuxt-images/partners/gcp/GCP.svg
            url: /partners/technology-partners/google-cloud-platform/
            aria_label: Link zur Partner-Fallstudie von GCP
          - name: AWS
            image: /nuxt-images/partners/aws/aws-logo.svg
            url: /partners/technology-partners/aws/
            aria_label: Link zur Partner-Fallstudie von AWS
