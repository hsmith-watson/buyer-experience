---
title: GitLab kostenlos testen
description: Nutze die 30-tägige kostenlose Testversion von GitLab Ultimate und erlebe den gesamten Lebenszyklus der Softwareentwicklung und das DevOps-Tool mit einer Vielzahl innovativer Funktionen.
components:
  - name: call-to-action
    data:
      title: GitLab Ultimate 30 Tage lang kostenlos ausprobieren
      centered_by_default: true
      subtitle: Die kostenlose Testversion enthält fast[[1]](#what-is-included-in-my-free-trial-what-is-excluded){class="cta__subtitle--subscript"} alle Funktionen des [Ultimate-Tarifs](/pricing/ultimate/){data-ga-name="Free trial includes nearly all Ultimate-tier" data-ga-location="header"}. Keine Kreditkarte erforderlich[[2]](/pricing/#why-do-i-need-to-enter-credit-debit-card-details-for-free-pipeline-minutes){class="cta__subtitle--subscript" data-ga-name="credit card faq" data-ga-location="header"}
      body_text: Starte jetzt deine 30-Tage-Testversion. Danach kannst du GitLab Free (für immer!) nutzen.
      aos_animation: fade-down
      aos_duration: 500
  - name: free-trial-plans
    data:
      saas:
        tooltip_text: |
          Software as a Service ist ein Softwarelizenzierungs- und Bereitstellungsmodell, bei dem Software auf Abonnementbasis lizenziert und zentral gehostet wird.
        text: |
          Wir hosten. Keine technische Einrichtung erforderlich. Du kannst sofort loslegen – es ist keine Installation erforderlich.
        link:
          text: Mit SaaS fortfahren
          href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=free-trial
          data_ga_name: continue with saas
          data_ga_location: body
        bottom_text: |
          Du hast bereits ein Konto? [Anmelden](https://gitlab.com/users/sign_in?redirect_to_referer=yes&__cf_chl_jschl_tk__=db2d336ba94805d0675008cc3fa3e0882d90953c-1619131501-0-AeQCSleOFTDGa9C-lXa3ZZZPpsO6sh0lCBCPZT0GxdT7tyOMAZoPzKppSQq9eV2Gqq9_kwKB8Lt8GJQ-nF-ra8updJRDfWTMBAwCR-m38kaHdAJYTicvW8Tj4KH55GO25zOeCYJexeEp1hx6f3DMvtjZd8elp_RfdulgN4-rxW8-lFSumJdSzE8y8N9FGltpsoQ8SKFSq41jMoB_GJ1nkIrjCU_kaGxJA3l4xhh-C14XFoBoBtfGjGOH4Kj76Y5QAeT7qemwuGBlvpYCK0OBv5aPkFDZ_Knp0W1zaOkr5tt511fra-rE3ekQI_lwR5VqBTHLtNslfgt4Il1SKLi6ZJLkces_WsUWdIQ3jNlyKbv08CF6kyDI3NiEOcCXUopCfQDYr-5syEUhv1Cnxy-Vjn7u5ejR2pvwIytWm8io2rhcaSOYxzxWccpxZLfjotTkzlrNP7KALbkxQOcNa_zeWVQ5t6aGC8H5wrT8u8ICxuJC){data-ga-name="log in" data-ga-location="body"}
      self_managed:
        form_id: 2150
        form_header: Du hostest. Lade GitLab herunter und installiere es auf deiner eigenen Infrastruktur oder in einer öffentlichen Cloud-Umgebung. Du brauchst Erfahrung mit Linux.
  - name: faq
    data:
      aos_animation: fade-up
      aos_duration: 500
      header: Häufig gestellte Fragen (FAQ) zur GitLab Testversion
      texts: 
        show: Zeige alles
        hide: Versteck alles
      groups:
        - header: Was ist in der kostenlosen Testversion enthalten?
          questions:
            - question: Was ist in meiner kostenlosen Testversion enthalten? Was ist ausgeschlossen?
              id: what-is-included-in-my-free-trial-what-is-excluded
              answer: |
                Deine kostenlose Testversion enthält fast alle Funktionen unseres [Ultimate-Tarifs](/pricing/){data-ga-name="ultimate tier" data-ga-location="faq"}, mit folgenden Ausnahmen:
                * Kostenlose Testversionen schließen Support auf allen Ebenen aus. Wenn du speziell die Support-Expertise oder SLA-Leistung von GitLab testen möchtest, [wende dich bitte an den Vertrieb](/sales/){data-ga-name="sales" data-ga-location="faq"}, um deine Optionen zu besprechen.
                * Kostenlose SaaS-Testversionen sind auf 400 Compute-Minuten pro Monat begrenzt.
                * SaaS Premium- und Ultimate-Testversionen ermöglichen die Verwendung eines einzigen [Projekt-Tokens](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html), das mit der Testlizenz verfügbar ist.
                * Kostenlose SaaS-Testversionen beinhalten nicht die Verwendung von [Gruppen-Token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html).
            - question: Was passiert nach Ablauf meiner kostenlosen Testversion?
              id: what-happens-after-my-free-trial-ends
              answer: |
                Deine Testversion von GitLab Ultimate hat eine Laufzeit von 30 Tagen. Nach diesem Zeitraum kannst du für immer ein GitLab Free-Konto beibehalten oder ein Upgrade auf einen [kostenpflichtigen Tarif](/pricing/){data-ga-name="paid plan" data-ga-location="faq"} durchführen.
        - header: SaaS vs. Self-Managed
          questions:
            - question: Was ist der Unterschied zwischen SaaS und Self-Managed Installationen?
              id: what-is-the-difference-between-saas-and-self-managed-setups
              answer: |
                SaaS: Wir hosten. Es ist kein technisches Setup erforderlich, sodass du dich nicht darum kümmern musst, GitLab selbst herunterzuladen und zu installieren. Self-Managed: Du hostest. Lade GitLab herunter und installiere es auf deiner eigenen Infrastruktur oder in unserer öffentlichen Cloud-Umgebung. Du brauchst Erfahrung mit Linux.
            - question: Sind bestimmte Funktionen nur in der SaaS- oder der Self-Managed-Version enthalten?
              id: are-certain-features-included-only-in-saas-or-self-managed
              answer: |
                Bestimmte Funktionen sind nur in der Self-Managed-Version verfügbar. Vergleiche die [vollständige Liste der Funktionen](/pricing/feature-comparison/){data-ga-name="features list" data-ga-location="faq"}.
        - header: Preise und Rabatte
          questions:
            - question: Ist für eine kostenlose Testversion eine Kredit-/Debitkarte erforderlich?
              id: is-a-credit-debit-card-required-for-a-free-trial
              answer: |
                Für Kund(inn)en, die GitLab.com CI/CD nicht verwenden, eigene Runner mitbringen oder gemeinsam genutzte Runner deaktivieren, ist keine Kredit-/Debitkarte erforderlich. Kredit-/Debitkarteninformationen sind jedoch erforderlich, wenn du dich für die Verwendung der gemeinsam genutzten Runner von GitLab.com entscheidest. Diese Änderung wurde vorgenommen, um den Missbrauch der auf GitLab.com zur Verfügung gestellten kostenlosen Compute-Minuten zum Mining von Kryptowährungen zu verhindern, da dies zu Performanceproblemen für Benutzer(innen) von GitLab.com führte. Wenn du eine Karte angibst, wird diese mit einer Autorisierung im Wert von einem US-Dollar überprüft. Es werden keine Gebühren erhoben und es wird kein Geld überwiesen. Weitere Informationen findest du [hier](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="prevent crypto mining abuse" data-ga-location="faq"}.
            - question: Wie viel kostet eine GitLab-Lizenz?
              id: how-much-does-a-gitlab-license-cost
              answer: |
                Informationen zu den Abonnements findest du auf der [Preis-Seite](/pricing/){data-ga-name="pricing page" data-ga-location="faq"}.
            - question: Gibt es Sonderpreise für Open-Source-Projekte, Startups oder Bildungseinrichtungen?
              id: do-you-have-special-pricing-for-open-source-projects--startups--or-educational-institutions
              answer: |
                Ja! Wir bieten kostenlose Ultimate-Lizenzen für qualifizierte Open-Source-Projekte, Startups und Bildungseinrichtungen. Weitere Informationen findest du auf unseren Programmseiten für [GitLab für Open Source](/solutions/open-source/){data-ga-name="gitlab for open source" data-ga-location="faq"}, [GitLab für Startups](/solutions/startups/){data-ga-name="gitlab for startups" data-ga-location="faq"} und [GitLab für Bildung](/solutions/education/){data-ga-name="gitlab for education" data-ga-location="faq"}.
            - question: Wie kann ich von GitLab Free auf eines der kostenpflichtigen Abonnements upgraden?
              id: how-do-i-upgrade-from-gitlab-free-to-one-of-the-paid-subscriptions
              answer: |
                Um ein Upgrade von GitLab Free auf einen der kostenpflichtigen Tarife durchzuführen, folge den [Anleitungen in unserer Dokumentation](https://docs.gitlab.com/ee/update/#community-to-enterprise-edition){data-ga-name="community to enterprise" data-ga-location="faq"}.
        - header: Installation und Migration
          questions:
            - question: Wie kann ich von einem anderen Git-Tool zu GitLab migrieren?
              id: how-do-i-migrate-to-gitlab-from-another-git-tool
              answer: |
                Alle Anleitungen zur Projektmigration für gängige Versionskontrollsysteme findest du in [unserer Dokumentation](https://docs.gitlab.com/ee/user/project/import/index.html){data-ga-name="migration" data-ga-location="faq"}.
            - question: Wie installiere ich GitLab mit einem Container?
              id: how-do-i-install-gitlab-using-a-container
              answer: |
                Informationen zur Installation von GitLab mit Docker findest du in [unserer Dokumentation](https://docs.gitlab.com/omnibus/docker/README.html){data-ga-name="install docker" data-ga-location="faq"}.
        - header: GitLab-Integrationen
          questions:
            - question: Ist GitHost noch verfügbar?
              id: is-githost-still-available
              answer: |
                Nein, wir akzeptieren keine neuen Kund(inn)en mehr für GitHost.
            - question: In welche Tools kann GitLab integriert werden?
              id: what-tools-does-gitlab-integrate-with
              answer: |
                GitLab bietet eine Reihe von Drittanbieter-Integrationen an. Weitere Informationen zu verfügbaren Diensten und deren Integration findest du in [unserer Dokumentation](https://docs.gitlab.com/ee/integration/README.html){data-ga-name="third party integration" data-ga-location="faq"}.
