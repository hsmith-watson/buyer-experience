---
  title: What is a code review?
  description: Code reviews ensure developers ship the highest quality code through systematic assessments designed to identify bugs.
  date_published: 2023-05-03
  date_modified: 2023-05-03
  topics_header:
    data:
      title: What is a code review?
      block:
        - metadata:
            id_tag: what-is-centralized-version-control-system
          text: A code review is a peer review of code that helps developers ensure or improve the code quality before they merge and ship it.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Version Control
      href: /topics/version-control/
      data-ga-name: version-control
      data_ga_location: breadcrumb
    - title: What is a code review?
  side_menu:
    anchors:
      text: "On this page"
      # data filled automatically by template
    hyperlinks:
      text: ''
      data: []
    content:
      - name: topics-copy-block
        data:
          header: What are code reviews, and how do they work?
          no_decoration: true
          enable_blockquote: true
          column_size: 11
          blocks:
            - text: |
                [Code reviews](/solutions/source-code-management/){data-ga-name="source code management" data-ga-location="body"}, also known as peer reviews, act as quality assurance of the code base.

                > Code reviews are methodical assessments of code designed to identify bugs, increase code quality, and help developers learn the source code.

                After a software developer has completed coding, a code review is an important step in the software development process to get a second opinion on the solution and implementation before it’s merged into an upstream branch like a feature branch or the main branch. The reviewer can also act as a second step in identifying bugs, logic problems, uncovered edge cases, or other issues.

                Reviewers can be from any team or group as long as they’re a domain expert. If the lines of code cover more than one domain, two experts should review the code.
      - name: topics-copy-block
        data:
          header: Why are code reviews important?
          column_size: 11
          blocks:
            - text: |
                Developing a strong code review process sets a foundation for continuous improvement and prevents unstable code from shipping to customers. Code reviews should become part of a software development team’s workflow to improve code quality and ensure that every piece of code has been looked at by another team member.

                The code review process is also an important part in spreading knowledge throughout an organization. For those reasons and more, 76% of developers who took the [2022 Global DevSecOps Survey](/developer-survey/){data-ga-name="developer survey" data-ga-location="body"} said code reviews are “very valuable.”
      - name: topics-copy-block
        data:
          header: What are the benefits of code reviews?
          column_size: 11
          blocks:
            - text: |
                - **Share knowledge:** When software developers review code as soon as a team member makes changes, they can learn new techniques and solutions. Code reviews help junior developers learn from more senior team members, similar to how pair programming effectively helps developers [share skills](/blog/2019/08/20/agile-pairing-sessions/) and ideas. By spreading knowledge across the organization, code reviews ensure that no person is a single point of failure. Everyone has the ability to review and offer feedback. Shared knowledge also helps team members take vacation, because everyone on the team has background knowledge on a topic.
                - **Discover bugs earlier:** Rather than discovering bugs after a feature has been shipped and scrambling to release a patch, developers can immediately find and fix problems before customers ever see them. Moving the review process earlier in the software development lifecycle through unit tests helps developers work on fixes with fresh knowledge. When waiting until the end of the lifecycle to do a review, developers often struggle to remember code, solutions, and reasoning. Static analysis is a cheap, [efficient](/blog/2020/09/08/efficient-code-review-tips/) way to meet business and customer value.
                - **Maintain compliance:** Developers have various backgrounds and training that influence their coding styles. If teams want to have a standard coding style, code reviews help everyone adhere to the same standards. This is especially important for open source projects that have multiple individuals contributing code. Peer reviews bring in maintainers to assess the code before pushing changes.
                - **Enhance security:** Code reviews create a high level of security, especially when security professionals engage in a targeted review. Application security is integral in software development, and code reviews help detect security issues and ensure compliance. Security team members can review code for vulnerabilities and alert developers to the threat. Code reviews are a great complement to automated scans and tests that detect security vulnerabilities.
                - **Increase collaboration:** When team members work together to create a solution, they feel more ownership of their work and a stronger sense of belonging. Authors and [reviewers](/blog/2020/10/13/merge-request-reviewers/) can work together to find the most effective solutions to meet customer needs. It’s important to strengthen collaboration across the software development lifecycle to prevent information silos and maintain a seamless workflow between teams. To successfully conduct code reviews, it’s important that developers build a code review [mindset](https://www.smashingmagazine.com/2019/06/bringing-healthy-code-review-mindset/) that has a strong foundation in collaborative development.
                - **Improve code quality:** Code reviews are an important way to ensure you ship high-quality code and quality software. A human who knows your code base can notice code quality issues that automated tests may miss. They can even help you reduce technical debt.
      - name: topics-copy-block
        data:
          header: What are the disadvantages of code reviews?
          column_size: 11
          blocks:
            - text: |
                - **Longer time to ship:** The review time could [delay](/blog/2020/07/03/challenges-of-code-reviews/) the release process, since reviewers have to [collaborate](/topics/version-control/software-team-collaboration/){data-ga-name="software team collaboration" data-ga-location="body"} with authors to discuss problems. Depending on a reviewer’s workload, they may not complete a review as fast as the author would like. This challenge can be overcome by using code review tools that include automated testing to find errors. Automated tooling is an effective way to free up developer time so that they can focus on the larger software engineering problems rather than highlight simple lint errors.
                - **Pull focus from other tasks:** Developers often have a heavy workload, and a code review can pull their focus away from other high priority tasks that they’re responsible for delivering. Team members may be forced to decide between completing their task or halting their work in order to do a code review. In either case, work is delayed somewhere across the organization. To reduce this pain point, team members can have a [reviewer roulette](/blog/2019/10/23/reviewer-roulette-one-year-on/) or a list of domain experts so that a single developer isn’t inundated with review requests.
                - **Large reviews mean longer review times:** If developers have to conduct code reviews on a large change, they could spend a significant amount of time examining the code. Large code reviews are challenging to assess, and developers may naturally move through the process quickly in order to complete it in a timely manner, resulting in decreased feedback quality. Incremental code development prevents this challenge by enabling reviewers to look at a small piece of code several times rather than a large change at once.
      - name: topics-copy-block
        data:
          header: Four approaches to code review
          column_size: 11
          blocks:
            - text: |
                Some of these disadvantages can be minimized by selecting the most appropriate code review method for your team. Here are four common approaches to code review:

                ### Pair programming

                Pair programming involves two developers collaborating in real time — one writing code (the driver) and one reviewing code (the navigator). Pairing sessions are popular with development teams because teammates collaborate to identify the most effective solution to a challenge. Team members share knowledge and can quickly overcome difficulties by working through ideas together and drawing on their expertise.

                **The benefits of pair programming**

                - Transfers knowledge
                - Prevents information silos
                - Solves complex problems
                - Increases morale
                - Finds more bugs
                - Can be conducted remotely

                **The drawbacks of pair programming**

                - Time-consuming
                - Can be overused
                - Difficult to measure

                ### Over-the-shoulder reviews

                In an over-the-shoulder-review, two developers — the author and reviewer — team up in person or remotely through a shared screen and the author explains the completed change proposal and offers reasoning for the chosen solutions. The reviewer asks questions and makes suggestions, similar to how team members collaborate during pairing sessions. The author can make small changes during the review and note larger fixes for a later time.

                **The benefits of over-the-shoulder reviews**

                - Easy implementation and completion
                - Can be conducted remotely
                - Faster than pair programming

                **The drawbacks of over-the-shoulder reviews**

                - Reviewer is detached from code
                - Review moves at the author’s pace
                - Lack of objectivity
                - No verification that changes were made
                - Difficult to measure

                ### Tool-assisted reviews

                Teams may decide to use tools to save time and ensure the highest quality code is shipped. Tool-assisted reviews can automatically gather changed files and display the differences, or make it easier to provide feedback and have conversations via comments, and incorporate things like static application security testing (SAST) to help identify and remediate vulnerabilities.

                The best way to look at tool-assisted reviews is to consider them a complement to other types of reviews. Automated tooling is an effective way to enforce code standards, identify vulnerability, gather metrics, and gather files, but some teams may be tempted to completely rely on tooling and forgo team member involvement to conduct code reviews. Tools should be viewed as an extension of code reviews and a way to enhance the process.

                **The benefits of tool-assisted reviews**

                - Easier to gather metrics
                - Automated tooling frees up developer focus

                **The drawbacks of tool-assisted reviews**

                - Developers must maintain tools
                - Expensive
                - Will still require teammate reviews

                ### Email pass-around

                Email pass-arounds are often used for minor difficulties and small pieces of code. They can be conducted via email or source code management systems. During an email pass-around, an author sends an email containing code changes to reviewers. Email pass-around is similar to over-the-shoulder reviews in that they can be easily implemented and don’t require a strong learning curve or a mentoring stage to teach the author how to make a change.

                **The benefits of email pass-arounds**

                - Easy implementation and completion
                - Facilitates remote, asynchronous reviews
                - Automatic reviews via SCMs

                **The drawbacks of email pass-arounds**

                - Time consuming to gather files
                - Difficult to follow conversations
                - No definite review end date
                - No verification that changes were made
                - Difficult to measure
      - name: topics-copy-block
        data:
          header: Some best practices for code reviews
          column_size: 11
          blocks:
            - text: |
                - **Limit code review sessions to keep them productive.** Figure out what works for your team — say, no more than one hour or 200 lines of code — and encourage them to stick to that limit.
                - **Include everyone — including new and senior members of the team — in the process.** Code reviews are an excellent way to help newer members of the team get up to speed with the code base — both by reviewing code from and having their code reviewed by more senior developers on the team. Including everyone in your code review process will also make it easier to adjust when people go on vacation or are no longer on the team.
                - **Distribute code review requests amongst the team.** It can be easy for a few developers to get the bulk of code review requests, and this won’t be good for them or the rest of the team — or the code base — long term. You can create a list of domain experts or a reviewer roulette to help avoid this.
                - **Ask questions and provide helpful context.** When you’re reviewing someone’s code, do your best to help both of you learn during the process. Not sure why they did something a different way than you might have? Ask. Have a suggestion for how to improve their code? Don’t forget to tell them why you’re suggesting it in your comment. This will not only help you both learn, but it will likely save time.
  components:
    - name: topics-cta
      data:
        subtitle: Discover how GitLab streamlines the code review process
        text: GitLab streamlines software development with comprehensive version control and collaboration.
        column_size: 10
        cta_one:
          text: Learn More
          link: /stages-devops-lifecycle/source-code-management/
          data_ga_name: Learn More
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Ready to learn more about code reviews?
        column_size: 4
        cards:
          - icon:
              name: ebook
              variant: marketing
            event_type: "eBook"
            header: Download the version control best practices eBook to enhance collaboration
            image: "/nuxt-images/resources/resources_10.jpeg"
            link_text: "Learn more"
            href: /resources/ebook-version-control-best-practices/
            data_ga_name: download the version control best practices eBook to enhance collaboration
            data_ga_location: resources
          - icon:
              name: book
              variant: marketing
            event_type: "Books"
            header: Watch how GitLab simplifies development with code review features
            image: "/nuxt-images/resources/resources_1.jpeg"
            link_text: "Learn more"
            href: https://page.gitlab.com/resources-demo-scm.html?_gl=1*17557ow*_ga*MTcxMzQ1MzM2Ni4xNjczMDE2Mjc3*_ga_ENFH3X7M5Y*MTY4Mjk3MjIzMi4yNzUuMS4xNjgyOTc1NDY0LjAuMC4w
            data_ga_name: watch how GitLab simplifies development with code review features
            data_ga_location: body
