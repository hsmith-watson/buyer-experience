---
  title: What is cloud native continuous integration?
  description:  Cloud native development needs continuous integration that supports speed. See what separates cloud native CI from regular CI.
  topic_name: Cloud native continuous integration
  icon: cloud-server
  date_published: 2023-04-11
  date_modified: 2023-04-11
  topics_header:
    data:
      title: What is cloud native continuous integration?
      block:
          - text: |
                In modern software development, most teams are already practicing continuous integration (CI). As DevOps teams look to increase velocity and scale, they look to cloud computing to help them achieve those goals. This kind of development is called cloud native development. These two concepts, CI and cloud native, work together so that teams can deploy to different environments.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Cloud Native Continuous Integration
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Cloud native + continuous integration
          href: "#cloud-native--continuous-integration"
          data_ga_name: cloud native + continuous integration
          data_ga_location: side-navigation
          variant: primary
        - text: What a cloud native CI pipeline needs
          href: "#what-a-cloud-native-ci-pipeline-needs"
          data_ga_name: what a cloud native ci pipeline needs
          data_ga_location: side-navigation
          variant: primary
    hyperlinks:
      text: ''
      data: []
    content:
      - name: 'topics-copy-block'
        data:
          header: Cloud native + continuous integration
          column_size: 10
          blocks:
              - text: |
                    [Cloud native](https://about.gitlab.com/topics/cloud-native/) is a way to build and run applications that take advantage of the scalability of the cloud computing model. Cloud native computing uses modern cloud services, like container orchestration, serverless, and [multicloud](https://about.gitlab.com/topics/multicloud/) to name a few. Cloud native applications are built to run in the cloud.


                    [CI](/topics/ci-cd/) is the practice of integrating code into a shared repository and building/testing each change automatically, several times per day. For teams using [pipeline as code](https://about.gitlab.com/topics/ci-cd/pipeline-as-code/), they can configure builds, tests, and deployment in code that is trackable and stored in the same shared repository as their source code.


                    Cloud native continuous integration is simply continuous integration that can supports cloud services often used in cloud native development.
      - name: 'topics-copy-block'
        data:
          header: What a cloud native CI pipeline needs
          column_size: 10
          blocks:
              - text: |
                   Cloud native offers opportunities in terms of velocity and scale, but also [increases complexity](https://thenewstack.io/the-shifting-nature-of-ci-cd-in-the-age-of-cloud-native-computing/). Cloud native engineering teams need increased automation and stability, and CI/CD tools designed to support the complexity that comes from developing in a [microservices](https://about.gitlab.com/topics/microservices/) environment.


                    For better cloud native development, teams should ensure their continuous integration solutions are optimized for the cloud services they commonly use:


                    * Container orchestration tools, like [Kubernetes](https://about.gitlab.com/solutions/kubernetes/), allow developers to coordinate the way in which an application’s containers will function, including scaling and deployment. For teams using Kubernetes, their cloud native CI should have a robust Kubernetes integration to support adding and/or managing multiple clusters.

                    * Seamless [continuous delivery](https://about.gitlab.com/stages-devops-lifecycle/continuous-delivery/) (CD), in addition to continuous integration, is important for cloud native and microservices development. High-functioning deployment strategies, like [canary deployments](https://docs.gitlab.com/ee/user/project/canary_deployments.html), can help cloud native teams test new features with the same velocity they use to build them.

                   * Cloud native applications are often architectured using microservices instead of a monolithic application structure, and rely on containers to package the application’s libraries and processes for deployment. A cloud native CI tool with [built-in container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html) can help streamline this process.

                   Cloud native continuous integration is designed to support the cloud services and architectures cloud native teams use, and offers the automation teams need for speed and stability.
  components:
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: blog
              alt: Blog icon
              variant: marketing
            event_type: "Blog post"
            header: "How to use GitLab for Agile portfolio planning and project management"
            link_text: "Read more"
            fallback_image: /nuxt-images/blogimages/nvidia.jpg
            href: "https://about.gitlab.com/blog/2020/11/11/gitlab-for-agile-portfolio-planning-project-management/"
          - icon:
              name: blog-alt
              variant: marketing
              alt: Blog Icon
            event_type: "Blog post"
            header: "How to create a CI/CD pipeline with Auto Deploy to Kubernetes using GitLab and Helm"
            link_text: "Read more"
            href: "https://about.gitlab.com/blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/"
            image: "/nuxt-images/home/resources/NIST.png"
            alt: City at night
            data_ga_name: "how to create a ci/cd pipeline with auto deploy to kubernetes using gitLab and helm"
            data_ga_location: resource cards
          - icon:
              name: blog
              alt: Blog Icon
              variant: marketing
            event_type: "Blog post"
            header: "How to use GitLab for Agile, CI/CD, GitOps, and more"
            link_text: "Read more"
            href: "https://about.gitlab.com/blog/2020/12/17/gitlab-for-cicd-agile-gitops-cloudnative/"
            image: "/nuxt-images/dedicated/dedicated-blog-header.png"
            data_ga_name: "how to use GitLab for Agile, CI/CD, GitOps, and more"
            data_ga_location: resource cards
          - icon:
              name: docs
              variant: marketing
              alt: docs Icon
            event_type: "Doc"
            header: Deploy software from GitLab CI/CD pipelines to Kubernetes
            link_text: "Learn More"
            image: "/nuxt-images/resources/resources_1.jpeg"
            href: https://docs.gitlab.com/ee/user/project/clusters/
            data_ga_name: Deploy software from GitLab CI/CD pipelines to Kubernetes
            data_ga_location: resource cards
          - icon:
              name: whitepapers
              alt: Whitepapers icon
              variant: marketing
            event_type: "Whitepaper"
            header: "How to deploy on AWS from GitLab"
            link_text: "Read More"
            fallback_image: /nuxt-images/blogimages/vlabsdev_coverimage.jpg
            href: "https://about.gitlab.com/resources/whitepaper-deploy-aws-gitlab/"
