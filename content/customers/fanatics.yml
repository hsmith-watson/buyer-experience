---
  title: Fanatics
  description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
  image_title: /nuxt-images/blogimages/fanatics_case_study_image.png
  image_alt: GitLab offers Fanatics the CI stability they were searching for
  twitter_image: /nuxt-images/blogimages/fanatics_case_study_image.png
  data:
    customer: Fanatics
    customer_logo: /nuxt-images/customers/Fanatics_logo2.svg
    heading: GitLab offers Fanatics the CI stability they were searching for
    key_benefits:
      - label: Improved CI stability
        icon: continuous-integration
      - label: Improved job scheduling
        icon: clock-alt
      - label: Increased user happiness
        icon: values
    header_image: /nuxt-images/blogimages/fanatics_case_study_image.png
    customer_industry: Retail
    customer_employee_count: 10,000
    customer_location: Jacksonville, FL; San Mateo, CA; Manchester, England; Boulder, CO
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: projects migrated in three months
        stat: 800
      - label: users
        stat: 300
      - label: user happiness rating
        stat: 95%
    blurb: Fanatics improved CI stability by moving to GitLab.
    introduction: |
      Fanatics’ successful GitLab CI transition empowers innovation cycles and speed
    content:
      - title: Bringing the game home to fans
        description: |
          Fanatics is a sports retailer that operates more than 300 online and offline stores. Fanatics offers a tech-infused approach to the world’s largest collection of sports team apparel and jerseys. Their mission is to amplify pride and create connections for all sports fans.
      - title: Going around in circles
        description: |
          The Fanatics cloud team has approximately 20 members tasked with all operations pertaining to cloud services and DevOps — including any Amazon Web Services (AWS) integrations. The integration team leader is responsible for running the continuous integration and continuous delivery (CI/CD) pipelines for Fanatics. In late 2018, the team was burdened with working on ongoing issues, such as patches and putting out fires. “It was not a very happy experience,” said Guilherme Goncalves, cloud tech lead. “The support was not very good. We had to solve all the issues ourselves.”

          Most of his time was spent fixing patches and working to solve issues with their legacy tooling, which included CircleCI. The issues were directly impacting the cloud team and slowed down release times, stopped the deployment of block ends, and caused memory leaks. The whole CI flow was unstable, especially impacting the cloud team.

          The Fanatics team spun up proofs-of-concept with several tools, including Travis and CodeBuild, but they ended up discarding them due to issues such as vendor lock-in, performance, flexibility, and scalability. This burdened Goncalves’ role to the point that his boss said that if he found a solution, a better tool, then he could make the call to make the switch.
      - title: Finding stability in a unique tool
        description: |
          Goncalves took his time to find a tool that had the same performance values as their existing tool, but included a level of stability that other tools couldn’t provide. “I researched everything. I looked at the CI tools out there, and I found GitLab and I loved it,” he said.

          He was the company’s biggest advocate for driving change. He created a GitLab channel, made demos, and asked all the important questions ahead of time. He also implemented multiple proofs-of-concept in his research for a tool that would be stable and integrate seamlessly into the existing infrastructure. Goncalves encouraged people to experiment for themselves, he said, because GitLab is very powerful.

          After the push from Goncalves, the decision was made to move over to GitLab at the end of 2018. It took the team about three months to make the entire transition and fully migrate 800 projects. There are now 300 users and about 60 teams using GitLab for CI. “I would say my first three months the cloud team was shifted to entirely GitLab," he said. "It was a good investment because GitLab is now running and I don’t need to take care of it anymore.”
      - title: Gaining time to focus on business-differentiating efforts
        description: |
          With GitLab, the cloud team at Fanatics has gained the ability to focus on innovation instead of worrying about patches and constant issues. “Because GitLab is much more stable, we are able to focus on Fanatics-specific challenges, rather than basic infrastructure issues,” Goncalves said. GitLab’s support system is responsive and transparent, so if and when issues arise, there is help.

          The team has started to put more focus on continuous deployments, now that they don’t have to constantly put out fires in the CI world. They can also schedule jobs, which is a feature that CircleCI doesn’t offer. Team members are able to work with group-level environment variables, empowering them to experiment more with workflows and schedule jobs. The benefits to moving to GitLab’s stable CI not only increased the ability to deliver, but allowed the development and engineering teams to collaborate more efficiently.

          Some teams are experimenting with GitLab’s source code management capabilities and are exploring how GitLab can help with continuous deployment into the future.

          Overall, Goncalves believes that GitLab would receive an approval rating of over 90% from fellow users at Fanatics. “Everyone is just happy that their builds are running in a timely fashion and stable enough that they never fail,” Goncalves said.
