---
  title: "Paris World Tour"
  og_title: Paris World Tour
  description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  twitter_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_image: /nuxt-images/events/world-tour/cities/Paris illustration.png
  twitter_image: /nuxt-images/events/world-tour/cities/Paris illustration.png
  time_zone: Eastern European Time (EET)
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: Paris
    header: Paris
    subtitle: October 17, 2023
    location: |
      Laho Business Center
      5-9 Rue Van Gogh
      75012 Paris, France

    image:
      src: /nuxt-images/events/world-tour/cities/Paris illustration.png
    button:
      text: Register for Paris
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  agenda:
    - time: 8:00 am
      name: Registration & Breakfast
      speakers:
    - time: 9:00 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Diane O'Neal
          title: Director, Product & Solutions Marketing
          company: GitLab
          image:
            src:  /nuxt-images/sixteen/diane-o-neal-picture.png
          biography:
            text: 
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us at the GitLab DevSecOps World Tour in Berlin where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape. Learn how Artificial Intelligence (AI) is set to impact all teams across the SDLC and how we envision organizations confidently securing their software supply chain while staying laser focused on delivering business value.

    - time: 9:30 am
      name: Next up for The DevSecOps Platform
      speakers:
        - name: Mike Flouton
          title: VP, Product Management
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Mike Flouton.jpg
          biography:
            text: Mike Flouton, VP of Product Management at GitLab, leads the CI/CD, technical enablement, and SaaS product teams. With nearly 25 years in cybersecurity, cloud, SaaS and enterprise software, Mike previously served as VP of Products at Barracuda Networks and VP of Product Marketing at BAE Systems. He is also an angel investor and startup advisor. A lifelong technology enthusiast, Mike began his career as a software engineer and holds a BS from Cornell University.
        - name: Fabian Zimmer
          title: Director, Product Management - SaaS Platforms
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Fabian Zimmer.jpg
          biography:
            text: Fabian has a background in Bioinformatics where he analyzed large amounts of genomic and transcriptomic data. He is passionate about building products that solve interesting problems and enjoys working in a fast-paced environment. At GitLab, Fabian is responsible for GitLab's SaaS Platforms section, including GitLab.com and GitLab Dedicated. The SaaS Platforms section aims to create tools, services, and frameworks that make building and deploying GitLab a delightful experience on any hardware at any scale.
        
      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster — and software development processes are undergoing transformative changes. With recent AI innovations, we see teams developing, securing and deploying software very differently. Join us to hear how our vision for GitLab is set to change how teams function across the entire SDLC, followed by a deep dive into our latest innovations where you will learn how to keep security at the forefront powered by intelligent automations.
    - time: 10:15 am
      name: Break
    - time: 10:30 am
      name: In conversation with Jordan Dubié, Thales Digital Factory
      speakers:
        - name: Philippe Charrière
          title: Customer Success Engineer
          company: Gitlab
          image:
            src: /nuxt-images/events/world-tour/speakers/Philippe Charrière.png
            contain: true
          biography:
            text: |
              Philippe is working as a Customer Success Engineer at GitLab, helping people with their GitLab adoption and usage.

              He loves programming with JavaScript, GoLang and Java, and He's taking his baby steps with Rust. His main playground is WebAssembly (mainly on the server side).

              Philippe is a GitLab lover (A legend says he was the first person to use emojis in a CI/CD pipeline file to name the stages and the jobs)

              Above all, he likes to explain simply complicated things.
        - name: Jordan Dubié
          title: Chief Product Owner
          company: Thales Digital Factory
          biography:
            text: Jordan is the Chief Product Owner for the software engineering environment and practices, deploying a global secured platform for all group engineers. 
                  Previously the leader of the group's Inner Source initiative, he is putting collaboration at the center of the engineering transformation at Thales.
          image: 
            src: /nuxt-images/events/world-tour/speakers/Jordan Dubié.jpg
            position: center
      description:
          text: Join us for this fireside chat and hear from Thales Digital Factory on how they successfully transformed their DevOps processes. You will gain insights on how they approached roadblocks and challenges which in turn helped them to emerge victorious in their DevSecOps journey. You will also learn how they effectively implemented DevSecOps best practices into their process to help their teams work smarter and deliver software faster. In this unique opportunity we also dig deeper into how leading organizations respond to rapidly evolving business needs and economic conditions, with minimal impact to their delivery velocity.
    - time: 11:15 pm
      name: Lunch & Networking
    - time: 12:30 pm
      name: |
        **Workshop:** How to optimize your DevSecOps workflow by focusing on Value Streams
      speakers:
        - name: Jonathan Fullam
          title: VP of Global Solutions Architecture
          company: GitLab
          biography:
            text: Currently, Jonathan leads a global team of value delivery and technical experts as Vice President of Solutions Architecture at GitLab, focusing on helping enterprises drive their business-oriented outcomes by realizing new capabilities and efficiencies within their operating model, technology strategy, and software delivery and management processes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jonathan Fullam.jpeg
        
        - name: Simon Mansfield
          title: Senior Manager, Solutions Architecture
          company: GitLab
          biography:
            text: |
              Simon is a seasoned DevOps and value stream expert with over 17 years in the tech industry. As the Senior Manager of Solutions Architecture at GitLab, he specializes in optimizing software development and delivery processes through cutting-edge DevOps practices. A sought-after speaker, Simon frequently shares his insights on DevOps, automation, and CI/CD pipelines at international conferences. Connect with him on LinkedIn and Twitter for his latest thoughts on the future of software development.
          image:
            src: /nuxt-images/events/world-tour/speakers/Simon Mansfield.jpg
            contain: true

        - name: Paul Dumaitre
          title: Solutions Architect
          company: GitLab
          biography:
            text: 
              When Paul is not busy debugging pipelines or lost in an ocean of browser tabs, he is helping teams practice DevSecOps.
              Value Stream Mapping enthusiast and champion of ethical AI.
          image:
            src: /nuxt-images/events/world-tour/speakers/Paul Dumaitre.jpg

        - name: Madou Coulibaly
          title: Senior Solutions Architect
          company: GitLab
          biography:
            text: |
              JA seasoned software developer and a passionate speaker with a decade of successful experience, Madou is a Solutions Architect helping organizations to understand all beneficial outcomes they will achieve by adopting a right DevSecOps strategy.
          image:
            src: /nuxt-images/events/world-tour/speakers/Madou Coulibaly.jpeg
        
      description:
        text: |
          If your team is looking to optimize your DevOps practices, join the DevOps Value Stream experts for valuable insights and actionable strategies. This interactive workshop will focus on discovery, identification and removal of blockers to drive visibility and continuous improvement. Dive deeper into Value Stream Assessments (VSA) and uncover areas of improvement in the SDLC. You will learn how you can accelerate value streams for faster business value realization. This interactive exercise will also provide practical experience for you to apply these VSA methodologies to your teams to identify key areas for improvement. At the end of this session, you will have a solid understanding of VSA principles, practical tools and techniques to set you ahead on the path to improving your DevOps processes.
    - time: 2:00 pm
      name: Break
    - time: 2:15 pm
      name: |
        Partner Spotlight: Spectrum Groupe
      speakers:
        - name: Spectrum Groupe
          title: '  '
          company: '  '
          image:
            src: /nuxt-images/events/world-tour/speakers/Spectrum.png
            contain: true
          biography:
            text: |
      
      description:
          text: Join us and hear from Spectrum Groupe on how they are uniquely positioned to power cloud innovation and deliver customer value alongside business transformation. Learn how, with the combined strength of GitLab and Spectrum Groupe, customers are achieving their business goals with an accelerated pace of innovation and faster time to market.
    
    - time: 2:40 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      description:
          text: |
            ‘Technology isn’t the hard problem anymore, culture seems to be the chief problem now.’- Is this a statement that you have come across?

            We invite you to discuss with leaders the role and impact of people on DevOps processes. Dig deeper into why communication, collaboration and empathy play a critical role in effective DevOps strategies. What are the strategies that work while introducing DevOps into businesses. Does it work if you find the core problem the organization faces and start there? Is education on agile a great place to start when thinking about building a DevOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability and  develop a human-centered design for DevOps processes.

    - time: 3:10 pm
      name: Closing Remarks
      description:
          text:
      speakers:
        - name: Julien Le Postec
          title: Senior Area Sales Manager
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Julien-Le-Postec.jpg
          biography:
            text: Sales leader and business development professional growing Gitlab's footprint in South EMEA Mid-Market segment.
    - time: 3:30 pm
      name: Snacks + Networking
  sponsors:
    - img:  /nuxt-images/events/world-tour/speakers/Spectrum.png
      alt: Spectrum Groupe logo image
  form:
    header: Register for DevSecOps World Tour in Paris
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: 3662
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
