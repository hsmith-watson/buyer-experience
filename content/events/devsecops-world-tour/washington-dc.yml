---
  title: "Washington, D.C. World Tour"
  og_title: Washington, D.C. World Tour
  description: Join us at the GitLab DevSecOps World Tour in Washington, D.C. where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  og_description: Join us at the GitLab DevSecOps World Tour in Washington, D.C. where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  twitter_description: Join us at the GitLab DevSecOps World Tour in Washington, D.C. where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  og_image: /nuxt-images/events/world-tour/cities/washington-llustration.png
  twitter_image: /nuxt-images/events/world-tour/cities/washington-llustration.png
  time_zone: Eastern Time (ET)
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: Washington, D.C.
    header: Washington, D.C.
    subtitle: October 25, 2023
    location: |
      JW Marriott Washington, D.C.
      1331 Pennsylvania Avenue NW
      Washington, D.C. 20004
    image:
      src: /nuxt-images/events/world-tour/cities/washington-llustration.png
    button:
      text: Register for Washington, D.C.
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
    expandHeader: true
  agenda:
    - time: 9:30 am
      name: Registration & Breakfast
      speakers:
    - time: 10:00 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Ashley Kramer
          title: Chief Marketing and Strategy Officer
          company: GitLab
          image:
            src:  /nuxt-images/events/world-tour/speakers/ashley-kramer.jpg
          biography:
            text: |
              Ashley Kramer is GitLab’s Chief Marketing and Strategy Officer. Ashley leverages her leadership experience in marketing, product, and technology to position GitLab as the leading DevSecOps platform. She also leads the strategy for product-led growth and code contribution to the GitLab platform.

              Prior to GitLab, Ashley was CPO and CMO of Sisense and has held several leadership roles, including SVP of Product at Alteryx and Head of Cloud at Tableau, as well as marketing, product, and engineering leadership roles at Amazon, Oracle, and NASA.
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us at the GitLab DevSecOps World Tour in Washington, D.C. where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape. Learn how Artificial Intelligence (AI) is set to impact all teams across the SDLC and how we envision organizations confidently securing their software supply chain while staying laser focused on delivering business value.
    - time: 10:30 am
      name: Next up for The DevSecOps Platform
      speakers:
        - name: Justin Farris
          title: Senior Director, Product
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Justin Farris.jpeg
          biography:
            text: Justin Farris is currently the Senior Director of Product at GitLab. With a background in Product Management, Strategy and Growth across numerous verticles. Justin brings a wealth of knowledge to help teams grow, scale & succeed. Prior to his role at GitLab, Justin served as the Director of Product at Zillow overseeing Growth teams.
        - name: Josh Lambert
          title: Director of Product Management, Enablement
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Josh Lambert.png
          biography:
            text: Josh's passion with computers started in elementary school when his parents brought an IBM PCjr home along with a book on BASIC. In his teenage years he spent time working on a variety of coding projects and assembling his own computers. He developed a love for product management to help solve problems with technology, and has been working in the DevSecOps space with GitLab for over 6 years. When not spending time with his family or working on technology, he tries to get out skiing and playing tennis.
      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster — and software development processes are undergoing transformative changes. With recent AI innovations, we see teams developing, securing and deploying software very differently. Join us to hear how our vision for GitLab is set to change how teams function across the entire SDLC, followed by a deep dive into our latest innovations where you will learn how to keep security at the forefront powered by intelligent automations.
    - time: 11:15 am
      name: Break
    - time: 11:30 am
      name: In discussion with Manuel Gauto from the US Navy
      speakers:
        - name: Lee Faus
          title: Global Field CTO
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Lee Faus.png
          biography:
            text: |
              Lee has worked with Senior Executives at Fortune 100 and Global 2000 companies for the past 10 years as a trusted advisor around Cloud Adoption, Agile Methodologies, DevSecOps and Engineering Best Practices.  He leverages his experience as an educator to bring complex concepts into a business forum where executives gain valuable advice that can provide immediate impact to their business.  Lee has given numerous keynotes at conferences internationally, speaking from his experience working across different verticals and vocalizing the challenges of running highly performant engineering teams.  Lee brings work experience from Borland, Compuware, Red Hat, Alfresco, GitHub, and GitLab and marries that with his consulting experiences at State Farm, Pfizer, Travelport, WellsFargo and Nokia to leverage lessons learned from other executives.  While Lee loves to talk about technology, he is also very passionate about measuring business outcomes and ensuring the efficiency of engineering teams while meeting customer demands.
        - name: Manuel Gauto
          title: US Navy
          company: US Navy
          biography:
            text: ''
      description:
        text: Join us for this fireside chat and hear from the US Navy on how they successfully transformed their DevOps processes. You will gain insights on how they approached roadblocks and challenges which in turn helped them to emerge victorious in their DevSecOps journey. You will also learn how they effectively implemented DevSecOps best practices into their process to help their teams work smarter and deliver software faster. In this unique opportunity we also dig deeper into how leading organizations respond to rapidly evolving business needs and economic conditions, with minimal impact to their delivery velocity.
    - time: 12:15 pm
      name: Lunch & Networking
    - time: 1:30 pm
      name: |
        **Workshop:** How to optimize your DevSecOps workflow by focusing on Value Streams
      logo:
        src: /nuxt-images/events/world-tour/CPE_Eligible-All_Black.png
        alt: cpe eligible badge
      speakers:
        - name: Jeremy Wagner
          title: Senior Solutions Architect
          company: GitLab
          biography:
            text: Jeremy is a Senior Solutions Architect with a background in Software Engineering and DevOps. He has experience in fast-burn startups as well as compliance-heavy enterprise companies. He enjoy solving complex customer challenges and delivering solutions with empirical results. He is a lifelong learner and passionate about all things technology, automation, and innovation.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jeremy Wagner.jpg
        - name: Jonathan Fullam
          title: VP of Global Solutions Architecture
          company: GitLab
          biography:
            text: Currently, Jonathan leads a global team of value delivery and technical experts as Vice President of Solutions Architecture at GitLab, focusing on helping enterprises drive their business-oriented outcomes by realizing new capabilities and efficiencies within their operating model, technology strategy, and software delivery and management processes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jonathan Fullam.jpeg
        - name: Ron Koster
          title: Solutions Architect
          company: GitLab
          biography:
            text: Located in the greater Boston area, Ron Koster joined GitLab in 2019 as a solution architect. Prior to GitLab, Ron operated in both pre and post sales capacity supporting DevOps for Database as well as change management for ERP systems technologies.
          image:
            src: /nuxt-images/events/world-tour/speakers/Ron Koster.jpeg
        - name: Kevin Chasse
          title: ''
          company: GitLab
          biography:
            text: ''
        - name: Josh Swann
          title: ''
          company: GitLab
          biography:
            text: ''
      description:
        text: |
          If your team is looking to optimize your DevOps practices, join the DevOps Value Stream experts for valuable insights and actionable strategies. This interactive workshop will focus on discovery, identification and removal of blockers to drive visibility and continuous improvement. Dive deeper into Value Stream Assessments (VSA) and uncover areas of improvement in the SDLC. You will learn how you can accelerate value streams for faster business value realization. This interactive exercise will also provide practical experience for you to apply these VSA methodologies to your teams to identify key areas for improvement. At the end of this session, you will have a solid understanding of VSA principles, practical tools and techniques to set you ahead on the path to improving your DevOps processes.
        blurb: We are pleased to offer 1.8 continuing professional education (CPE) credits to those that attend the event. CPE certification at DevSecOps World Tour is being administered by Carahsoft Technology Corp. Carahsoft is registered with the National Association of State Boards of Accountancy (NASBA) as a sponsor of continuing professional education of the National Registry of CPE Sponsors. For more information on the CPE credits we are offering, the CPE sponsor, NASBA, and submission process, please <a href="https://carahevents.carahsoft.com/Event/Details/402988-CPE" data-ga-name="cpe credits vsa" data-ga-location="body">click here</a>.
    - time: 3:00 pm
      name: Break
    - time: 3:15 pm
      name: |
        Partner Spotlight: Carahsoft
      speakers:
        - name: ''
          title: '  '
          company: '  '
          image:
            src: /nuxt-images/events/world-tour/sponsors/Carahsoft-Blue-Logo-Web.png
            contain: true
          biography:
            text: ''
      description:
        text: |
          Join us and hear from Carahsoft on how they are uniquely positioned to power cloud innovation and deliver customer value alongside business transformation. Learn how, with the combined strength of GitLab and Carahsoft, customers are achieving their business goals with an accelerated pace of innovation and faster time to market.
    - time: 3:40 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      speakers:
        - name: Jeremy Wagner
          title: Senior Solutions Architect
          company: GitLab
          biography:
            text: Jeremy is a Senior Solutions Architect with a background in Software Engineering and DevOps. He has experience in fast-burn startups as well as compliance-heavy enterprise companies. He enjoy solving complex customer challenges and delivering solutions with empirical results. He is a lifelong learner and passionate about all things technology, automation, and innovation.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jeremy Wagner.jpg
      description:
          text: |
            ‘Technology isn’t the hard problem anymore, culture seems to be the chief problem now.’- Is this a statement that you have come across?

            We invite you to discuss with leaders the role and impact of people on DevOps processes. Dig deeper into why communication, collaboration and empathy play a critical role in effective DevOps strategies. What are the strategies that work while introducing DevOps into businesses. Does it work if you find the core problem the organization faces and start there? Is education on agile a great place to start when thinking about building a DevOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability and  develop a human-centered design for DevOps processes.
    - time: 4:10 pm
      name: Closing Remarks
    - time: 4:30 pm
      name: Snacks + Networking
  sponsors:
    - img: /nuxt-images/events/world-tour/sponsors/Carahsoft-Blue-Logo-Web.png
      alt: carahsoft logo
      blurb: Carahsoft Technology Corp. is The Trusted Government IT Solutions Provider®, supporting Public Sector organizations across Federal, State and Local Government and Education and Healthcare. As the Master Government Aggregator® for our vendor partners, we deliver solutions for Cybersecurity, MultiCloud, DevSecOps, Big Data, Artificial Intelligence, Open Source, Customer Experience and Engagement, and more. Working with our reseller partners, our sales and marketing teams provide industry leading IT products, services and training through hundreds of contract vehicles.
  form:
    multi_step: true
    header: Register for DevSecOps World Tour in Washington, D.C.
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: '3923'
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
  additionalLogo:
      src: /nuxt-images/events/world-tour/CPE_Eligible-All_Black.png
      alt: cpe eligible badge
  blurb: "We are pleased to offer 3.8 continuing professional education (CPE) credits to those that attend the event. CPE certification at DevSecOps World Tour is being administered by Carahsoft Technology Corp. Carahsoft is registered with the National Association of State Boards of Accountancy (NASBA) as a sponsor of continuing professional education of the National Registry of CPE Sponsors. For more information on the CPE credits we are offering, the CPE sponsor, NASBA, and submission process, please <a href='https://carahevents.carahsoft.com/Event/Details/402981-CPE' data-ga-name='cpe credits' data-ga-location='body'>click here</a>."